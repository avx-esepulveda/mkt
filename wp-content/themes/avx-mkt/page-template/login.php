<?php /* Template Name: Login */ ?>
<?php get_header(); ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-4">
            <div class="content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-4">
            <?php $args = array(
                'redirect' => home_url(), 
                'form_id' => 'loginform-custom',
                'label_username' => __( 'Usuario' ),
                'label_remember' => __( 'Recordar mis datos' ),
                'label_log_in' => __( 'Iniciar sesión' ),
                'remember' => true
            );?>
            <?php wp_login_form($args); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>