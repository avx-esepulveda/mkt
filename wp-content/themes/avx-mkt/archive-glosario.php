<?php get_header() ?>
    <div class="container">
        <div class="text-center">
            <h1>Glosario Marketing Digital</h1>
        </div>
        <?php $args = array(
            'post_type' => 'glosario',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'name'
        );
        $query_glosario = new WP_Query( $args );
        if ( $query_glosario->have_posts() ) { ?>
            <?php while ( $query_glosario->have_posts() ) { $query_glosario->the_post(); ?>
                <div class="item mb-4">
                    <h4><?= get_the_title(); ?></h4>
                    <?php the_excerpt(); ?>
                </div>
            <?php } wp_reset_postdata(); ?>   
        <?php } ?>
    </div>
<?php get_footer(); ?>