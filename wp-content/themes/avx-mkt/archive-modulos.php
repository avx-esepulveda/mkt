<?php get_header() ?>

<?php $modulos_description = wpautop(custom_theme_cpt_get_option('modulos' , 'archive_description_home')); 

$tax = 'category'; ?>

    <section class="modules">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="description">
                        <?= $modulos_description ?>
                    </div>
                </div>
            </div>
            <div class="tax-list">
                <div class="row">
                <?php 
                    $tax_terms = get_terms($tax,array(
                        'hide_empty' => 1,
                        'exclude' => array( 1 )
                    ));?>
                    <?php foreach ($tax_terms as $tax_term) {
                        $term_link = get_term_link( $tax_term );
                        $tax_id = $tax_term->term_id;
                        $excerpt_category = wpautop(get_term_meta( $tax_id , 'avx_term_excerpt', 1 ));
                        $icon_category = get_term_meta( $tax_id , 'avx_term_icon', 1 );
                        $image_category = get_term_meta( $tax_id , 'avx_term_thumbnail', 1 ); ?>
                        <div class="col-12 col-sm-6">
                            <div class="item">
                                <div class="row no-gutters">
                                    <div class="col-12 col-sm-6">
                                        <figure>
                                            <a href="<?= esc_url( $term_link )?>">
                                                <img src="<?= $image_category?>" class="img-fluid d-block mx-auto" alt="<?= $tax_term->name?>">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <h3> <span><img src="<?= $icon_category ?>" class="img-fluid" alt=""></span><a href="<?= esc_url( $term_link )?>">
                                            <?= $tax_term->name;?>
                                        </a></h3>
                                        <?= $excerpt_category ?>
                                        <div class="wrapper-btn text-right">
                                            <a href="<?= esc_url( $term_link )?>" class="btn btn-orange"><i class="fa fa-star" aria-hidden="true"></i>¡Comencemos!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>