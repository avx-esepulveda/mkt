<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class Theme_admin {

	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = 'theme_options';

	 /**
     * Array of metaboxes/fields
     * @var array
     */
    protected $option_metabox = array();

	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	//private $metabox_id = 'theme_option_metabox';

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = array();

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'Theme Options', 'avx-ktm' );
		
	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_init', array( $this, 'option_fields' ) );
	}


	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		$option_tabs = self::option_fields();
		 foreach ($option_tabs as $index => $option_tab) {
        	register_setting( $option_tab['id'], $option_tab['id'] );
        }
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$option_tabs = self::option_fields();
		foreach ($option_tabs as $index => $option_tab) {
        	if ( $index == 0) {
				$this->options_page[] = add_menu_page( 
					$this->title, 
					$this->title, 
					'manage_options', 
					$option_tabs[0]['id'], 
					array( $this, 'admin_page_display' )
				);
				add_action( "admin_print_styles-{$this->options_page[$index]}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
				// Include CMB CSS in the head to avoid FOUT
			}else {
        		$this->options_page[] = add_submenu_page( 
					$option_tabs[0]['id'], 
					$this->title, 
					$option_tab['title'], 
					'manage_options', 
					$option_tab['id'], 
					array( $this, 'admin_page_display' ) 
				);
				add_action( "admin_print_styles-{$this->options_page[$index]}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
				
        	}
        }
	}
	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		$option_tabs = self::option_fields(); //get all option tabs
    	$tab_forms = array();     	   	
		?>
		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<!-- Options Page Nav Tabs -->           
            <h2 class="nav-tab-wrapper">
            	<?php foreach ($option_tabs as $option_tab) :
            		$tab_slug = $option_tab['id'];
            		$nav_class = 'nav-tab';
            		if ( $tab_slug == $_GET['page'] ) {
            			$nav_class .= ' nav-tab-active'; //add active class to current tab
            			$tab_forms[] = $option_tab; //add current tab to forms to be rendered
            		}
            	?>            	
            	<a class="<?php echo $nav_class; ?>" href="<?php menu_page_url( $tab_slug ); ?>"><?php esc_attr_e($option_tab['title']); ?></a>
            	<?php endforeach; ?>
            </h2>
            <!-- End of Nav Tabs -->
			<?php foreach ($tab_forms as $tab_form) : //render all tab forms (normaly just 1 form) ?>
            <div id="<?php esc_attr_e($tab_form['id']); ?>" class="group">
            	<?php cmb2_metabox_form( $tab_form, $tab_form['id'],array( 'cmb_styles' => false ) ); ?>
				<p><em>Guardar es independiente por cada tab</em></p>
            </div>
            <?php endforeach; ?>
		</div>
		<?php

	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	public function option_fields() {

		global $lang; 

		// Only need to initiate the array once per page-load
        if ( ! empty( $this->option_metabox ) ) {
            return $this->option_metabox;
        } 

		//place here the variable arrays for group type fields
		//add fields for each tab in an array
		$header_fields = array(
			array(
				'name' => __( 'Website logo', 'avx-ktm' ),
				'desc' => __( 'JPG, PNG, SVG image', 'avx-ktm' ),
				'id'   => 'logo',
				'type' => 'file'
			),
			array(
				'name' => __( 'Favicon', 'avx-ktm' ),
				'desc' => __( 'JPG or PNG image', 'avx-ktm' ),
				'id'   => 'favicon_image',
				'type' => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 57x57', 'avx-ktm' ),
				'id'      => 'apple_icon_57',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 72x72', 'avx-ktm' ),
				'id'      => 'apple_icon_72',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 114x114', 'avx-ktm' ),
				'id'      => 'apple_icon_114',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 144x144', 'avx-ktm' ),
				'id'      => 'apple_icon_144',
				'type'    => 'file'
			),
		);
		$footer_fields = array(
			array(
				'name' => __( 'Contact Information', 'avx-ktm' ),
				'id'   => 'titleContact',
				'type' => 'title'
			),
			array(
				'name' => __( 'Text', 'avx-ktm' ),
				'id'   => 'contactFooter',
				'type' => 'wysiwyg'
			),
			array(
				'name' => __( 'Latitude', 'avx-ktm' ),
				'id'   => 'latitude',
				'type' => 'text'
			),
			array(
				'name' => __( 'Longitude', 'avx-ktm' ),
				'id'   => 'longitude',
				'type' => 'text'
			),

		); 
		$redes_fields = array(
			array(
				'name'    => __( 'Facebook', 'avx-ktm' ),
				'id'      => 'fb_link',
				'type'    => 'text_url'
			),
			array(
				'name'    => __( 'Youtube', 'avx-ktm' ),
				'id'      => 'yt_link',
				'type'    => 'text_url'
			),
			array(
				'name'    => __( 'Instagram', 'avx-ktm' ),
				'id'      => 'in_link',
				'type'    => 'text_url'
			),
		);
		$home_fields = array(
			array(
				'name' => __( 'Main Slider', 'avx-ktm' ),
				'desc' => __( 'Shortcode Revolution Slider', 'avx-ktm' ),
				'id'   => 'titleMainBanner',
				'type' => 'title'
			),
			array(
				'name' => __( 'Shortcode', 'avx-ktm' ),
				'id'   => 'mainBanner',
				'type' => 'text'
			),
			/*  */
			array(
				'name' => __( 'Equipo', 'avx-ktm' ),
				'id'   => 'titleTeam',
				'type' => 'title'
			),
			array(
				'name' => __( 'Descripción', 'avx-ktm' ),
				'id'   => 'desc_team',
				'type' => 'wysiwyg'
			),
			array(
				'name' => __( 'Imagen', 'avx-ktm' ),
				'id'   => 'img_team',
				'type' => 'file'
			),

			/*  */
			array(
				'name' => __( 'Equipo', 'avx-ktm' ),
				'id'   => 'titleWhatIs',
				'type' => 'title'
			),
			array(
				'name' => __( 'Descripción Izquierda', 'avx-ktm' ),
				'id'   => 'left_WhatIs',
				'type' => 'wysiwyg'
			),
			array(
				'name' => __( 'Descripción Derecha', 'avx-ktm' ),
				'id'   => 'right_WhatIs',
				'type' => 'wysiwyg'
			),
			array(
				'name' => __( 'Link', 'avx-ktm' ),
				'id'   => 'link_btn',
				'type' => 'text_url'
			),
		); 
		$checkout_fields = array(
			array(
				'name' => __( 'Link to Allianz', 'avx-ktm' ),
				'id'   => 'allianz_link_title',
				'type' => 'title'
			),
			array(
				'name' => __( 'Link to Allianz', 'avx-ktm' ),
				'desc' => __( 'allianz supplemental insurance url', 'avx-ktm' ),
				'id'   => 'allianz_link',
				'type' => 'text_url',
			),

			array(
				'name' => __( 'Waiver forms link', 'avx-ktm' ),
				'id'   => 'allianz_link_title',
				'type' => 'title'
			),
			array(
				'name' => __( 'Link to Allianz', 'avx-ktm' ),
				'desc' => __( 'allianz waiver forms url', 'avx-ktm' ),
				'id'   => 'waiver_link',
				'type' => 'text_url',
			),
		); 

		// Set our CMB2 tabs
		$this->option_metabox[] = array(
			'id'      => 'header_options',
			'title'   => 'Header',
			'hookup'  => false,
			'show_on' => array(
				'key'   => 'options-page',
				'value' => array( 'header_options'),
			),
			'show_names' => true,
			'fields' =>  $header_fields,
		);
		$this->option_metabox[] = array(
			'id'      => 'home_options',
			'title'   => 'Home',
			'hookup'  => false,
			'show_on' => array(
				'key'   => 'options-page',
				'value' => array( 'home_options'),
			),
			'show_names' => true,
			'fields' =>  $home_fields,
		);
		$this->option_metabox[] = array(
			'id'      => 'footer_options',
			'title'      => 'Footer',
			'hookup'  => false,
			'show_on' => array(
				'key'   => 'options-page',
				'value' => array( 'footer_options'),
			),
			'show_names' => true,
			'fields' => $footer_fields,
		) ;
		$this->option_metabox[] = array(
			'id'      => 'social_options',
			'title'   => 'Social Networks',
			'hookup'  => false,
			'show_on' => array(
				'key'   => 'options-page',
				'value' => array( 'social_options'),
			),
			'show_names' => true,
			'fields' =>  $redes_fields,
		);
		/*$this->option_metabox[] = array(
			'id'      => 'checkout_options',
			'title'   => 'Checkout fields',
			'hookup'  => false,
			'show_on' => array(
				'key'   => 'options-page',
				'value' => array( 'checkout_options'),
			),
			'show_names' => true,
			'fields' =>  $checkout_fields,
		);*/

		//insert extra tabs here

		return $this->option_metabox;
	}
	/**
     * Returns the option key for a given field id
     * @since  0.1.0
     * @return array
     */
    public function get_option_key($field_id) {
    	$option_tabs = $this->option_fields();
    	foreach ($option_tabs as $option_tab) { //search all tabs
    		foreach ($option_tab['fields'] as $field) { //search all fields
    			if ($field['id'] == $field_id) {
    				return $option_tab['id'];
    			}
    		}
    	}
    	return $this->key; //return default key if field id not found
    }

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		if ( 'option_metabox' === $field ) {
            return $this->option_fields();
        }
		throw new Exception( 'Invalid property: ' . $field );
	}

}


/**
 * Helper function to get/return the theme_admin object
 * @since  0.1.0
 * @return theme_admin object
 */
function theme_admin() {
	static $object = null;
	if ( is_null( $object ) ) {
		$object = new Theme_admin();
		$object->hooks();
	}

	return $object;
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function theme_get_option( $key = '' ) {
	return cmb2_get_option( theme_admin()->get_option_key($key), $key );
}
// Get it started
theme_admin();