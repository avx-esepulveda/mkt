<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class Custom_Theme_CPT_Admin {

	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = '';

	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	private $metabox_id = '';

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';
	
	/**
	 * This post Type
	 * @var string
	 */
	protected $post_type = '';

	/**
	 * Holds an instance of the object
	 *
	 * @var Myprefix_Admin
	 **/
	private static $instance = null;

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct( $post_type ) {
		// Set our vars
		$this->title = 'Options';
		$this->key = $post_type . '_options';
		$this->post_type = $post_type;
		$this->metabox_id = $post_type . '_options';
		
		add_action( 'admin_init', array( &$this, 'init' ) );
		add_action( 'admin_menu', array( &$this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( &$this, 'add_options_page_metabox' ) );
	}

	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page = add_submenu_page( 'edit.php?post_type=' . $this->post_type, $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );

		// Include CMB CSS in the head to avoid FOUC
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
	}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		?>
		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>
		<?php
	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	function add_options_page_metabox() {

		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );
		$custom = new_cmb2_box( array(
	        'id'         => $this->metabox_id,
			'hookup'     => false,
			'cmb_styles' => false,
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
	    ) );
		/*$custom->add_field( array(
	    	'name' => 'Title',
	    	'id' => 'archive-title',
	    	'type' => 'text',
		) );
		$custom->add_field( array(
	    	'name' => 'Content',
	    	'id' => 'archive-content',
	    	'type' => 'wysiwyg',
		) );*/

	}

	/**
	 * Register settings notices for display
	 *
	 * @since  0.1.0
	 * @param  int   $object_id Option key
	 * @param  array $updated   Array of updated fields
	 * @return void
	 */
	public function settings_notices( $object_id, $updated ) {
		if ( $object_id !== $this->key || empty( $updated ) ) {
			return;
		}

		add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'myprefix' ), 'updated' );
		settings_errors( $this->key . '-notices' );
	}

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}

		throw new Exception( 'Invalid property: ' . $field );
	}

}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $post_type Post type name
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function custom_theme_cpt_get_option( $post_type, $key ) {
	return cmb2_get_option( $post_type . '_options' , $key );
}