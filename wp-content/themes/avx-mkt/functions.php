<?php

// Initialize the metabox class
if ( file_exists( dirname( __FILE__ ) . '/lib/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/lib/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/CMB2/init.php';
}

/* OPCIONES DEL TEMA */
require_once ( get_template_directory() . '/admin/theme-options.php' );
require_once ( get_template_directory() . '/admin/custom-theme-types.php' );
//secure login para el sitio
/*if ( file_exists( dirname( __FILE__ ) . '/functions/secure.php' ) ) {
    require_once ( get_template_directory() . '/functions/secure.php' );
}*/

function add_header_xframeoptions() {
    header( 'X-Frame-Options: SAMEORIGIN' );
}
add_action( 'send_headers', 'add_header_xframeoptions' );

/* SVG */
function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );


/* Reference Styles and JS */
function theme_enqueue_style() {
    wp_enqueue_style( 'appcss', get_stylesheet_directory_uri() .'/assets/css/app.css', array(), '', 'all');
    wp_enqueue_style( 'avx-mktcss', get_stylesheet_directory_uri() .'/assets/css/avx-mkt.css', array(), '2.6', 'all');
}
function wpb_adding_scripts() {
    wp_enqueue_script( 'fontawesome','https://use.fontawesome.com/bdb876848a.js', '', true );
    wp_enqueue_script( 'modernizr',get_stylesheet_directory_uri() . '/assets/dist/modernizr-2.8.3-respond-1.4.2.min.js','','',true);
    wp_enqueue_script( 'appjs',get_stylesheet_directory_uri() . '/assets/js/app.js','','',true);
    wp_enqueue_script( 'classie',get_stylesheet_directory_uri() . '/assets/dist/classie.js','','',true);
    wp_enqueue_script( 'mainjs',get_stylesheet_directory_uri() . '/assets/js/avx-mkt.js','','',true);
    wp_enqueue_script( 'menuJS',get_stylesheet_directory_uri() . '/assets/js/menu.js','','',true);
    wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js','','',true);
    wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js','','',true);

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );  

/*Custom logo in login */
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo theme_get_option( 'logo' ); ?>);
            padding-bottom: 0px;
            width: 100%;
            height: 150px;
            -webkit-background-size: 90%;
            -moz-background-size: 90%;
            -o-background-size: 90%;
            background-size: 90%;
            background-position: center center;
        }
    </style>
<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url_title() {
    return get_bloginfo('name');
}

/* SOPORTE PARA IMAGENES EN EL POST Y REMOVER LOS ATRIBUTOS WIDHT & HEIGHT*/
if (function_exists('add_theme_support')) { 
    add_theme_support('post-thumbnails');
}

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

/* Registro Menus */
function register_my_menus() {
  register_nav_menus(
    array(
        'main-menu'     => 'Main Menu',
    )
  );
}
add_action( 'init', 'register_my_menus' );

/** Excerpt Page **/
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/** PAGINADOR */
function base_pagination() {
    global $wp_query;

    $big = 999999999; // This needs to be an unlikely integer

    // For more options and info view the docs for paginate_links()
    // http://codex.wordpress.org/Function_Reference/paginate_links
    $paginate_links = paginate_links( array(
        'base'          => str_replace( $big, '%#%', get_pagenum_link($big) ),
        'current'       => max( 1, get_query_var('paged') ),
        'total'         => $wp_query->max_num_pages,
        'mid_size'      => 5,
        'prev_next'     => True,
        'prev_text'     => __('<i class="fa fa-angle-double-left" aria-hidden="true"></i>'),
        'next_text'     => __('<i class="fa fa-angle-double-right" aria-hidden="true"></i>'),
        'type'          => 'list'
    ) );

    // Display the pagination if more than one page is found
    if ( $paginate_links ) {
        echo '<div class="clear"></div><div id="paginador" class="text-center">';
        echo $paginate_links;
        echo '</div><!--// end .pagination -->';
    }
}
/* Paginador WP_query */
function pagination_query($query = null) {
    if ( !$query ) {
        global $wp_query;
        $query = $wp_query;
    }
     
    $big = 999999999; // need an unlikely integer
 
    $pagination = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var( 'paged' ) ),
        'total' => $query->max_num_pages,
        'mid_size' => 5,
        'prev_next'    => True,
        'prev_text'    => __('<i class="fa fa-angle-double-left" aria-hidden="true"></i>'),
        'next_text'    => __('<i class="fa fa-angle-double-right" aria-hidden="true"></i>'),
        'type'         => 'list'
            ) );
    ?>
    
    <div class="clear"></div>
    <div id="paginador" class="text-center">
            <?php echo $pagination; ?>
    </div> 
    <?php
}

/*Register CPT */
function create_post_type() {
    register_post_type( 'modulos',
        array(
            'labels' => array(
                'name'               => _x( 'Módulos', 'post type general name', 'avx-mkt' ),
                'singular_name'      => _x( 'Módulos', 'post type singular name', 'avx-mkt' ),
                'menu_name'          => _x( 'Módulos', 'admin menu', 'avx-mkt' ),
                'name_admin_bar'     => _x( 'Módulos', 'add new on admin bar', 'avx-mkt' ),
                'add_new'            => _x( 'Add New', 'post', 'avx-mkt' ),
                'add_new_item'       => __( 'Add New Post', 'avx-mkt' ),
                'new_item'           => __( 'New Post', 'avx-mkt' ),
                'edit_item'          => __( 'Edit Post', 'avx-mkt' ),
                'view_item'          => __( 'View Post', 'avx-mkt' ),
                'all_items'          => __( 'All Post', 'avx-mkt' ),
                'search_items'       => __( 'Search Post', 'avx-mkt' ),
                'parent_item_colon'  => __( 'Parent Post:', 'avx-mkt' ),
                'not_found'          => __( 'No post found.', 'avx-mkt' ),
                'not_found_in_trash' => __( 'No post found in Trash.', 'avx-mkt' )
            ),
            'public'        => true,
            'menu_position' => 10,
            'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'has_archive'   => false,
            'hierarchical'  => true,
            'menu_icon'     => 'dashicons-pressthis',
            'show_in_rest'  => true
        )
    );
    register_post_type( 'ejercicios',
        array(
            'labels' => array(
                'name'               => _x( 'Ejercicios', 'post type general name', 'avx-mkt' ),
                'singular_name'      => _x( 'Ejercicios', 'post type singular name', 'avx-mkt' ),
                'menu_name'          => _x( 'Ejercicios', 'admin menu', 'avx-mkt' ),
                'name_admin_bar'     => _x( 'Ejercicios', 'add new on admin bar', 'avx-mkt' ),
                'add_new'            => _x( 'Add New', 'post', 'avx-mkt' ),
                'add_new_item'       => __( 'Add New Post', 'avx-mkt' ),
                'new_item'           => __( 'New Post', 'avx-mkt' ),
                'edit_item'          => __( 'Edit Post', 'avx-mkt' ),
                'view_item'          => __( 'View Post', 'avx-mkt' ),
                'all_items'          => __( 'All Post', 'avx-mkt' ),
                'search_items'       => __( 'Search Post', 'avx-mkt' ),
                'parent_item_colon'  => __( 'Parent Post:', 'avx-mkt' ),
                'not_found'          => __( 'No post found.', 'avx-mkt' ),
                'not_found_in_trash' => __( 'No post found in Trash.', 'avx-mkt' )
            ),
            'public'        => true,
            'menu_position' => 10,
            'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'has_archive'   => false,
            'hierarchical'  => true,
            'menu_icon'     => 'dashicons-pressthis',
            'show_in_rest'  => true
        )
    );
    register_post_type( 'proyectos',
        array(
            'labels' => array(
                'name'               => _x( 'proyectos', 'post type general name', 'avx-mkt' ),
                'singular_name'      => _x( 'proyectos', 'post type singular name', 'avx-mkt' ),
                'menu_name'          => _x( 'proyectos', 'admin menu', 'avx-mkt' ),
                'name_admin_bar'     => _x( 'proyectos', 'add new on admin bar', 'avx-mkt' ),
                'add_new'            => _x( 'Add New', 'post', 'avx-mkt' ),
                'add_new_item'       => __( 'Add New Post', 'avx-mkt' ),
                'new_item'           => __( 'New Post', 'avx-mkt' ),
                'edit_item'          => __( 'Edit Post', 'avx-mkt' ),
                'view_item'          => __( 'View Post', 'avx-mkt' ),
                'all_items'          => __( 'All Post', 'avx-mkt' ),
                'search_items'       => __( 'Search Post', 'avx-mkt' ),
                'parent_item_colon'  => __( 'Parent Post:', 'avx-mkt' ),
                'not_found'          => __( 'No post found.', 'avx-mkt' ),
                'not_found_in_trash' => __( 'No post found in Trash.', 'avx-mkt' )
            ),
            'public'        => true,
            'menu_position' => 10,
            'supports'      => array( 'title', 'editor'),
            'has_archive'   => true,
            'hierarchical'  => true,
            'menu_icon'     => 'dashicons-editor-help',
            'show_in_rest'  => true
        )
    );
    register_post_type( 'glosario',
        array(
            'labels' => array(
                'name'               => _x( 'Glosario', 'post type general name', 'avx-mkt' ),
                'singular_name'      => _x( 'Glosario', 'post type singular name', 'avx-mkt' ),
                'menu_name'          => _x( 'Glosario', 'admin menu', 'avx-mkt' ),
                'name_admin_bar'     => _x( 'Glosario', 'add new on admin bar', 'avx-mkt' ),
                'add_new'            => _x( 'Add New', 'post', 'avx-mkt' ),
                'add_new_item'       => __( 'Add New Post', 'avx-mkt' ),
                'new_item'           => __( 'New Post', 'avx-mkt' ),
                'edit_item'          => __( 'Edit Post', 'avx-mkt' ),
                'view_item'          => __( 'View Post', 'avx-mkt' ),
                'all_items'          => __( 'All Post', 'avx-mkt' ),
                'search_items'       => __( 'Search Post', 'avx-mkt' ),
                'parent_item_colon'  => __( 'Parent Post:', 'avx-mkt' ),
                'not_found'          => __( 'No post found.', 'avx-mkt' ),
                'not_found_in_trash' => __( 'No post found in Trash.', 'avx-mkt' )
            ),
            'public'        => true,
            'menu_position' => 10,
            'supports'      => array( 'title', 'editor'),
            'has_archive'   => true,
            'hierarchical'  => true,
            'menu_icon'     => 'dashicons-book',
            'show_in_rest'  => true
        )
    );
}
add_action( 'init', 'create_post_type', 0 );

/*Register Taxonomies*/
function create_taxomies() {
    $labels = array(
    'name'              => _x( 'Categories', 'taxonomy general name', 'parces' ),
    'singular_name'     => _x( 'Category', 'taxonomy singular name', 'parces' ),
    'search_items'      => __( 'Search Categorys', 'parces' ),
    'all_items'         => __( 'All Categorys', 'parces' ),
    'parent_item'       => __( 'Parent Category', 'parces' ),
    'parent_item_colon' => __( 'Parent Category:', 'parces' ),
    'edit_item'         => __( 'Edit Category', 'parces' ),
    'update_item'       => __( 'Update Category', 'parces' ),
    'add_new_item'      => __( 'Add New Category', 'parces' ),
    'new_item_name'     => __( 'New Category Name', 'parces' ),
    'menu_name'         => __( 'Category', 'parces' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    'show_admin_column' => true,
    'show_in_rest'      => false
  );
  register_taxonomy( 'category', array('modulos'), $args );
}
add_action( 'init', 'create_taxomies', 0 );

function get_excerpt($count){
    $excerpt = get_the_excerpt();
    $excerpt = strip_tags($excerpt);
    $excerpt = mb_substr($excerpt, 0, $count);
    return $excerpt;
}

function add_image_class($class){
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class','add_image_class');

// Redirect users who arent logged in...
function members_only() {
    global $pagenow;
    //var_dump($pagenow);
    // Check to see if user in not logged in and not on the login page
    if( !is_user_logged_in() && !is_page('ingresar') ){
        wp_redirect( '/ingresar' );
        exit;
    }
    if ( is_user_logged_in() && is_page('ingresar') ){
        wp_redirect( home_url() );
        exit;
    }
}
add_action( 'wp', 'members_only' ); 

add_action( 'init', 'blockusers_init' );
    function blockusers_init() {
    if ( is_admin() && !current_user_can( 'administrator' ) && !( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_redirect( home_url() );
        exit;
    }
}

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );
function wti_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'main-menu') {
      if (is_user_logged_in()) {
         $items .= '<li class="right"><a href="'. wp_logout_url(home_url()) .'">'. __("Cerrar Sesión") .'</a></li>';
      } 
   }
   return $items;
}