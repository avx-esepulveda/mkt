<?php get_header(); ?>
<?php $slider = theme_get_option( 'mainBanner' );
/*  */
$desc_team = wpautop(theme_get_option( 'desc_team' ));
$img_team = theme_get_option( 'img_team' );
/*  */
$left_WhatIs = wpautop(theme_get_option( 'left_WhatIs' ));
$right_WhatIs = wpautop(theme_get_option( 'right_WhatIs' )); 
$link_ = theme_get_option( 'link_btn' ); 

$modulos_description = wpautop(custom_theme_cpt_get_option('modulos' , 'archive_description_home')); 

$tax = 'category'; 
?>
    <section class="slider">
            <?= do_shortcode($slider); ?>
    </section>

    <section class="team">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 align-self-center">
                    <div class="the-content">
                        <?= $desc_team ?>
                    </div>
                </div>
                <div class="col-12 col-sm-6 align-self-center">
                    <figure>
                        <img src="<?= $img_team ?>" class="img-fluid mx-auto d-block" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="whatIs">
        <div class="container mb-5">
            <div class="row justify-content-between">
                <div class="col-12 col-sm-6 col-md-6">   
                    <?= $left_WhatIs ?>
                </div>
                <div class="col-12 col-sm-6 col-md-4">
                    <?= $right_WhatIs ?>
                    <a href="<?= $link_ ?>" class="btn btn-block btn-orange">Mas información</a>
                </div>  
            </div>
        </div>
    </section>

    <section class="modules" id="modulos">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="description">
                        <?= $modulos_description ?>
                    </div>
                </div>
            </div>
            <div class="tax-list">
                <div class="row">
                <?php 
                    $tax_terms = get_terms($tax,array(
                        'hide_empty' => 1,
                        'exclude' => array( 1 )
                    ));?>
                    <?php foreach ($tax_terms as $tax_term) {
                        $term_link = get_term_link( $tax_term );
                        $tax_id = $tax_term->term_id;
                        $excerpt_category = wpautop(get_term_meta( $tax_id , 'avx_term_excerpt_home', 1 ));
                        $icon_category = get_term_meta( $tax_id , 'avx_term_icon', 1 );
                        $image_category = get_term_meta( $tax_id , 'avx_term_thumbnail_main', 1 ); ?>
                        <div class="col-12 col-sm-6 mb-5">
                            <div class="item">
                                <div class="row no-gutters h-100">
                                    <div class="col-12 col-sm-6 align-self-end">
                                        <figure>
                                            <a href="<?= esc_url( $term_link )?>">
                                                <img src="<?= $image_category?>" class="img-fluid d-block mx-auto" alt="<?= $tax_term->name?>">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <h3> <span><img src="<?= $icon_category ?>" class="img-fluid" alt=""></span><a href="<?= esc_url( $term_link )?>">
                                            <?= $tax_term->name;?>
                                        </a></h3>
                                        <?= $excerpt_category ?>
                                        <div class="wrapper-btn text-center text-md-right">
                                            <a href="<?= esc_url( $term_link )?>" class="btn btn-orange"><i class="fa fa-star" aria-hidden="true"></i>¡Comencemos!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>