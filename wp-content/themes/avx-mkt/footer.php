</main><!-- #main-wrapper -->
    <div class="clearfix"></div>
    <footer class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12 py-5">
                    <div class="legal text-center">
                        <p class="m-0">Todos los derechos reservados. <b>Avanxo <?= date('Y'); ?></b></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div><!-- #general-wrapper -->
<?php wp_footer(); ?>
</body>
</html>