<?php get_header(); ?>

<?php 
global $post; 
$tax                = 'category'; 
$currentterm        = $wp_query->queried_object->slug;
$currentterm_id     = $wp_query->queried_object->term_id;
$currentterm_name   = $wp_query->queried_object->name;
$post_type          = 'modulos';

$image_hero_banner = get_term_meta( $currentterm_id , 'avx_term_hero_banner', 1 );
$excerpt_hero_banner = wpautop(get_term_meta( $currentterm_id , 'avx_term_excerpt', 1 ));
$term_thumb_detail = get_term_meta( $currentterm_id , 'avx_term_thumbnail', 1 );
?>

<section class="hero-banner mb-5">
    <img src="<?= $image_hero_banner?>" alt="<?= $currentterm_name ?>" class="w-100 d-block mx-auto">
    <div class="the-content">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 offset-md-4 col-md-8">
                    <h1><?= $currentterm_name ?></h1>
                    <?= $excerpt_hero_banner ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="intro-modulo">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb">
                    <a href="<?= get_option('home'); ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                    <a href="/#modulos" rel="m_PageScroll2id">Módulos</a>
                    <i class="fa fa-angle-right"></i>
                    <span><?= $currentterm_name?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4 align-self-center">
                <figure>
                    <img src="<?= $term_thumb_detail ?>" alt="<?= $currentterm_name ?>" class="img-fluid d-block">
                </figure>
            </div>
            <div class="col-12 col-sm-8">
                <?= term_description( $currentterm_id, $tax ) ?>
            </div>
        </div>
    </div>
</section>
<section class="articles-list">
    <div class="container">
        <div class="row">
            <div class="col-12"><h5>¡COMENCEMOS!</h5></div>
        </div>
        <div class="row">
        <?php $args = array(
                'post_type'         => $post_type,
                'category'    => $currentterm,
                'posts_per_page'    => -1,
                'order'             => 'ASC',
            );
            $query_modulos = new WP_Query( $args );
            if ( $query_modulos->have_posts() ) {
                while ( $query_modulos->have_posts() ) { $query_modulos->the_post(); 
                    $thumbnail_main = get_post_meta($post->ID, 'avx_thumbnail_main', 1);?>
                    <div class="col-12 col-sm-6">
                        <article>
                            <div class="row no-gutters mb-5">
                                <div class="col-12 col-sm-5 align-self-end">
                                    <figure>
                                        <a href="<?php the_permalink() ?>">
                                            <img src="<?= $thumbnail_main ?>" class="img-fluid d-block mx-auto" alt="<?= get_the_title(); ?>">
                                        </a>
                                        <a href="<?php the_permalink() ?>" class="btn btn-orange"><i class="fa fa-star" aria-hidden="true"></i>¡Comencemos!</a>
                                    </figure>
                                </div>
                                <div class="col-12 col-sm-7">
                                    <div class="content">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <p><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php }
                    wp_reset_postdata();
                } ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>