var browserSync     = require('browser-sync');
var connect			= require('gulp-connect-php')
var gulp            = require("gulp");
var sass            = require('gulp-sass');
var gutil           = require("gulp-util");
var concat          = require("gulp-concat");
var mainBowerFiles  = require("main-bower-files");
var uglify          = require("gulp-uglify");
var streamqueue     = require('streamqueue');
var nano            = require('gulp-cssnano');
var minifyCss       = require('gulp-minify-css');

var debug = false;
var outputRoot = "";

gulp.task("lib_js", function () {
    debug = debug || false;

    var plugins = mainBowerFiles("**/*.js");
    if (debug) gutil.log(plugins);

    var src = gulp.src(plugins);
    if (!debug) src = src.pipe(uglify());

    return src
        .pipe(concat("app.js"))
        .pipe(gulp.dest("assets/js"));
});

gulp.task("lib_css", function () {
    debug = debug || false;
    return streamqueue({objectMode: true},
        gulp.src(mainBowerFiles("**/*.scss")).pipe(sass()),
        gulp.src(mainBowerFiles("**/*.css")))
        .pipe(concat('app.css')).pipe(nano()).pipe(gulp.dest("assets/css"))
});

gulp.task('appcss', function() {
    gulp.src('devcss/**/*.scss')
        .pipe(sass())
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(gulp.dest("assets/css"))
});

gulp.task('minjs', function() {
    gulp.src('assets/js/devjs/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest("assets/js"))
});

gulp.task("default", ["lib_js", "lib_css"],function() {
    gulp.watch('devcss/**/*.scss', ['appcss']);
} );

gulp.task("debug", function () {
    debug = true;
    gutil.log(gutil.colors.green('RUNNING IN DEBUG MODE'));
    gulp.start('default');
})