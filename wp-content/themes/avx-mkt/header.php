<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--FAVICON-->
    <link rel="icon" href="<?php echo theme_get_option( 'favicon_image' ); ?>" type="image/x-icon">
    <!-- APPLE TOUCH ICON-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_get_option( 'apple_icon_57' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_get_option( 'apple_icon_72' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_get_option( 'apple_icon_114' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_get_option( 'apple_icon_144' ); ?>">
    <?php wp_head(); ?>
</head>
<body>
<div class="overlay overlay-contentscale">
    <button type="button" class="overlay-close">X</button>
    <nav>
        <?php wp_nav_menu(array('menu_class'=> 'navbar-nav ml-auto','theme_location' => 'main-menu', 'container' => false));?>
    </nav>
</div>
<div <?php body_class( 'general-wrapper'); ?>>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?= get_option('home'); ?>">
                    <img class="img-fluid mx-auto d-block" src="<?= theme_get_option( 'logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>" />
                </a>
                <button id="trigger-overlay" class="toggler" type="button">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
        </nav>
    </header>
    <main id="main-wrapper">