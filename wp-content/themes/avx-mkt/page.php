<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <?php the_title(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <?php the_Content(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>