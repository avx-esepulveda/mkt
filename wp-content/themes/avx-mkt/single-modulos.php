<?php get_header() ?>

<?php
global $post;
$category = get_the_category();
$currentterm_name   = $wp_query->queried_object->name;
$thumb = get_post_meta(get_the_ID(), 'avx_thumbnail_main', 1);
$related = get_post_meta(get_the_ID(), 'avx_related_exe', 1);  ?>

<section class="hero-banner mb-5">
    <?php the_post_thumbnail('full', array('class' => 'img-fluid d-block w-100 mx-auto')); ?>
    <div class="the-content">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12 col-sm-4 order-last order-md-first">
                    <img src="<?= $thumb ?>" class="img-fluid d-block mx-auto thumb" >
                </div>
                <div class="col-12 col-sm-8 mb-0 mb-md-5 order-first order-md-last">
                    <?php the_title('<h1>','</h1>') ?>
                    <h2><?= $category[0]->name ?></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb">
                    <a href="<?= get_option('home'); ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                    <a href="/#modulos" rel="m_PageScroll2id">Módulos</a>
                    <i class="fa fa-angle-right"></i>
                    <a href="<?= get_category_link($category[0]->cat_ID) ?>"><?= $category[0]->name ?></a>
                    <i class="fa fa-angle-right"></i>
                    <span><?php the_title();?></span>
                </div>
            </div>
        </div>
        <div class="row justify-content-between">
            <div class="col-12 col-sm-9 col-md-8">
                <article>
                    <div class="intro-module">
                        <?php the_excerpt(); ?>
                    </div>
                    <?php the_content(); ?>
                </article>
            </div>
            <div class="col-12 col-sm-3 col-md-3">
                <aside>
                    <?php if ( $related ) { ?>
                        <?php $args = array(
                            'post_type' => 'ejercicios',
                            'posts_per_page' => -1,
                            'post__in' => $related,
                            'orderby' => 'post__in'
                        );
                        $query_ejercicios = new WP_Query( $args );
                        if ( $query_ejercicios->have_posts() ) { ?>
                            <h3>Ejercicios</h3>
                            <?php while ( $query_ejercicios->have_posts() ) { $query_ejercicios->the_post(); ?>
                                <div class="item mb-4">
                                    <h4><a href="<?php the_permalink() ?>"><?= get_the_title(); ?></a></h4>
                                    <a href="<?php the_permalink() ?>"><?php the_excerpt(); ?></a>
                            </div>
                            <?php } wp_reset_postdata(); ?>   
                        <?php } ?>
                    <?php } ?>
                </aside>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>