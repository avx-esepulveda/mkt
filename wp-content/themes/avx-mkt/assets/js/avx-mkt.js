(function ($) {
    function createNodes(list) {
        var result = {};
        for (var key in list) {
            result[key] = $(list[key]);
        }
        return result;
    };
    var selectors = {
        window: window,
        body: 'body',
        main: '#main-wrapper',
        header: "header",
    };
    var nodes;

    function addSticky() {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                nodes.header.addClass("sticky");
                //nodes.contentWrapper.addClass("sticky");
            } else {
                nodes.header.removeClass("sticky");
                //nodes.contentWrapper.removeClass("sticky");
            }
        });
    };

    function customCheckbox(element) {
        $(element).after('<span class="checkmark"></span>');
    };

    $(document).ready(function () {
        nodes = createNodes(selectors);
        addSticky();
        customCheckbox(".login-remember input[type=checkbox]");
    });
})(jQuery);