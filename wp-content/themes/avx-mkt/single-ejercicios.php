<?php get_header() ?>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10">
                <article>
                    <?php the_title('<h1>','</h1>') ?>
                    <?php the_content(); ?>
                </article>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>