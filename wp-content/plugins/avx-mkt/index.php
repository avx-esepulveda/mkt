<?php 
/*
Plugin Name: Metabox Plugin Avx
Description: Customs fields
Version: 0.1
Author: AVX
Author URI: http://avanxo.com/
*/

require_once plugin_dir_path( __FILE__ ) . '/lib/field-post/cmb-field-post-search-ajax.php';

/* Metabox cars */
add_action( 'cmb2_admin_init', 'metabox_modulos' );
function metabox_modulos() {
    $prefix = 'avx_';
    
	$cmb_thumb = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_thumbnail',
		'title'         => esc_html__( 'Thumbnail', 'mkt' ),
		'object_types'  => array( 'modulos' ), // Post type
        'context'    => 'side',
        'priority'  =>  'low',
		'show_names' => false,
	) );
	/* $cmb_thumb->add_field( array(
        'name'       => esc_html__( 'Detalle', 'mkt' ),
		'id'         => $prefix . 'thumbnail',
        'type'       => 'file',
	) ); */
	$cmb_thumb->add_field( array(
        'name'       => esc_html__( 'General', 'mkt' ),
		'id'         => $prefix . 'thumbnail_main',
        'type'       => 'file',
	) );

	$cmb_related_exe = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_related_exe',
		'title'         => esc_html__( 'Ejercicio Relacionado', 'mkt' ),
		'object_types'  => array( 'modulos' ), // Post type
        'context'    => 'normal',
        'priority'  =>  'low',
		'show_names' => false,
	) );	
	$cmb_related_exe->add_field( array(
		'name' => __( '', 'metabox-dynamik' ),
		'id'   => $prefix . 'related_exe',
		'type'      	=> 'post_search_ajax',
		'desc'			=> __( 'Escriba el título del ejercicio', 'metabox-dynamik' ),
		'limit'      	=> 10,
		'sortable' 	 	=> true,
		'query_args'	=> array(
			'post_type'			=> array( 'ejercicios' ),
			'post_status'		=> array( 'publish' ),
			'posts_per_page'	=> -1
		)
	));
}

/* Theme options CPT */
add_action( 'cmb2_admin_init', 'avx_register_theme_options_metabox' );
function avx_register_theme_options_metabox() {
	/**/
	$cmb_options_modulos = new_cmb2_box( array(
		'id'      => 'modulos_options',
		'title'   => esc_html__( 'Theme Options Metabox', 'applachian' ),
		'hookup'  => false, // Do not need the normal user/post hookup.
		'show_on' => array(
			'key'   => 'options-page',
			'value' => array( 'modulos_options' )
		),
	) );

	$cmb_options_modulos->add_field( array(
		'name'    => esc_html__( 'Descripción home', 'applachian' ),
		'id'      => 'titleDescription',
		'type'    => 'title',
    ) );
	$cmb_options_modulos->add_field( array(
		'name'    => esc_html__( '', 'applachian' ),
		'id'      => 'archive_description_home',
		'type'    => 'wysiwyg',
    ) );
	/**/
}

add_action( 'cmb2_admin_init', 'yourprefix_register_taxonomy_metabox' );
function yourprefix_register_taxonomy_metabox() {
	$prefix = 'avx_term_';
	/**
	 * Metabox to add fields to categories and tags
	 */
	$cmb_term = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => esc_html__( 'Category Metabox', 'cmb2' ), // Doesn't output for term boxes
		'object_types'     => array( 'term' ), // Tells CMB2 to use term_meta vs post_meta
		'taxonomies'       => array( 'category', 'post_tag' ), // Tells CMB2 which taxonomies should have these fields
		// 'new_term_section' => true, // Will display in the "Add New Category" section
	) );
	$cmb_term->add_field( array(
		'name'     => esc_html__( 'Extra Info', 'cmb2' ),
		'id'       => $prefix . 'extra_info',
		'type'     => 'title',	
		'on_front' => false,
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Excerpt', 'cmb2' ),
		'id'   => $prefix . 'excerpt',
		'type' => 'textarea',
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Excerpt home', 'cmb2' ),
		'id'   => $prefix . 'excerpt_home',
		'type' => 'textarea',
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Term Hero Banner', 'cmb2' ),
		'id'   => $prefix . 'hero_banner',
		'type' => 'file',
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Term Thumbnail Detail', 'cmb2' ),
		'id'   => $prefix . 'thumbnail',
		'type' => 'file',
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Term Thumbnail General', 'cmb2' ),
		'id'   => $prefix . 'thumbnail_main',
		'type' => 'file',
	) );
	$cmb_term->add_field( array(
		'name' => esc_html__( 'Term Icon', 'cmb2' ),
		'id'   => $prefix . 'icon',
		'type' => 'file',
	) );
	
}